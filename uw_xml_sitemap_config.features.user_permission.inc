<?php

/**
 * @file
 * uw_xml_sitemap_config.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_xml_sitemap_config_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer xmlsitemap'.
  $permissions['administer xmlsitemap'] = array(
    'name' => 'administer xmlsitemap',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'xmlsitemap',
  );

  // Exported permission: 'use xmlsitemap'.
  $permissions['use xmlsitemap'] = array(
    'name' => 'use xmlsitemap',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'xmlsitemap',
  );

  return $permissions;
}
